# coding=utf-8

class TrailingZeroes:
    def trailingZeroes(self, n):
        cnt = 0
        num = 5
        while n / num > 0:
            cnt += n / num
            num *= 5
        return cnt

if __name__ == '__main__':
     n = 6
     trailingZeroes = TrailingZeroes()
     print trailingZeroes.trailingZeroes(n)
