# coding=utf-8

class ArraySign:
    def arraySign(self, nums):
        sign = 1
        for num in nums:
            if num == 0:
                return 0
            if num < 0:
                sign = -sign
        return sign


if __name__ == '__main__':
    nums = [-1, -2, -3, -4, 3, 2, 1]
    arraySign=ArraySign()
    print arraySign.arraySign(nums)

