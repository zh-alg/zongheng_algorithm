# coding=utf-8

class ToLowerCase:
    def toLowerCase(self, s):
        """
        :type s: str
        :rtype: str
        """
        list1 = []
        for i in s:
            if ord('A') <= ord(i) <= ord('Z'):
                list1.append(chr(ord(i) + 32))
            else:
                list1.append(chr(ord(i)))
        return ''.join(list1)


if __name__ == '__main__':
    s = "Hello"
    toLowerCase = ToLowerCase()
    print toLowerCase.toLowerCase(s)
