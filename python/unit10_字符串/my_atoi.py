# coding=utf-8

class MyAtoi:
    def myAtoi(self, s):
        if s.isspace() or s == "":
            return 0
        s = s.strip(" ")
        # print(s)
        mid = s.split()
        # print(mid)
        char = 1
        sum1 = ""
        if s[0].isalpha():
            return 0
        for i in mid:
            if i[0].isalpha():
                continue
            elif i[0] == "-":
                target = i
                break
            elif i[0] == "+":
                target = i
                break
            elif i[0].isdigit():
                target = i
                break
            elif i[0] == ".":
                return 0
        if target[0].isdigit():
            for t in target:
                if t.isdigit():
                    sum1 += t
                else:
                    break
        if target[0] == "-":
            char = 0
            for t in target[1:]:
                if t.isdigit():
                    sum1 += t
                else:
                    break
        if target[0] == "+":
            for t in target[1:]:
                if t.isdigit():
                    sum1 += t
                else:
                    break
        if sum1 == "":
            return 0
        # #print(char)
        if char == 1:
            result = int(sum1)
        else:
            result = 0 - int(sum1)
        if result >= 2 ** (31) - 1:
            result = 2 ** (31) - 1
        elif result <= -2 ** (31):
            result = -2 ** (31)
        return result


if __name__ == '__main__':
    s = "   -42"
    myAtoi = MyAtoi()
    print myAtoi.myAtoi(s)
    s = "4193 with words"
    print myAtoi.myAtoi(s)
