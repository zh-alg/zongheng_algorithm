# coding=utf-8
if __name__ == '__main__':
    # my_str = """Hello, welcome to
    #       the world of Python"""
    # print my_str


    # utf-8与str字符串大小测试
    # s1 = "字节串"
    # print(type(s1))  # 输出　<type 'str'>，按照开头的encoding来编码成相应的字节
    # print(len(s1))  # 输出9，因为按utf8编码，一个汉字占3个字节，3个字就占9个字节
    #
    # s2 = u"万国码"
    # print(type(s2))  # 输出　<type 'unicode'>，用unicode编码，2个字节1个字符
    # print(len(s2))  # 输出3，unicode用字符个数来算长度，从这个角度上看，unicode才是真正意义上的字符串类型

    # print ("ABC".encode("utf-8"))
    s = '中文字符'
    print s[-2:] == '字符'