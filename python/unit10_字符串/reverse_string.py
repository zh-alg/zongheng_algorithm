# coding=utf-8

class ReverseString:
    def reverseString(self, s):
        s[:] = s[::-1]
        self.recursion(s, 0, len(s) - 1)

    def recursion(self, s, left, right):
        if left >= right:  # 2.直至头尾指针相等时返回回去
            return
        # 即从内向外开始交换元素 继而达到反转字符串效果
        self.recursion(s, left + 1, right - 1)
        s[left], s[right] = s[right], s[left]  # 交换


if __name__ == '__main__':
    s = ["h", "e", "l", "l", "o"]
    reverseString = ReverseString()
    reverseString.reverseString(s)
    print s
