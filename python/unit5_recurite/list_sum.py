# coding=utf-8
# 迭代实现累加
def list_sum(num_list):
    the_sum = 0
    for i in num_list:
        the_sum = the_sum + i
    return the_sum


if __name__ == '__main__':
    print(list_sum([1, 3, 5, 7, 9]))
