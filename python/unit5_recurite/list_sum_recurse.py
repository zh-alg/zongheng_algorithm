# coding=utf-8
# 迭代实现累加
def list_sum(num_list):
    if len(num_list) == 1:
        return num_list[0]
    else:
        return num_list[0] + list_sum(num_list[1:])


if __name__ == '__main__':
    print(list_sum([1, 3, 5, 7, 9]))
