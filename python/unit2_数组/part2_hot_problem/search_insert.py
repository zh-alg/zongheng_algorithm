class SearchInsert(object):
    def search_insert(self, nums, target):
        n = len(nums) - 1
        left = 0
        right = n - 1
        ans = n
        while left <= right:
            mid = int((right + left) / 2)
            if target <= nums[mid]:
                ans = mid
                right = mid - 1
            else:
                left = mid + 1
        return ans


if __name__ == '__main__':
    search_insert = SearchInsert()
    a = [1, 3, 5, 6]
    ans = search_insert.search_insert(a, 7)
    print(ans)
