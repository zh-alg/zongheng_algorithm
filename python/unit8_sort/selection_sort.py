# coding=utf-8
def selection_sort(alist):
    for i in range(len(alist)):
        mid_variable = i  # 定义一个中间变量
        for j in range(i + 1, len(alist)):
            if alist[mid_variable] > alist[j]:
                mid_variable = j
        alist[i], alist[mid_variable] = alist[mid_variable], alist[i]
    return alist


if __name__ == '__main__':
    alist = [10, 21, 32, 12, 56, 45, 89, 78, 566]
    print(selection_sort(alist))
