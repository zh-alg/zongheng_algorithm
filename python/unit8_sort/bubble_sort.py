# coding=utf-8
# 冒泡排序
def bubble_sort(A):
    le = len(A)  # 获取元素数目
    for i in range(le - 1):  # 遍历le-1次
        for j in range(i, le):  # 每一趟冒泡，待排序元素数目都会-1
            if A[j] > A[j - 1]:  # 逆序则交换
                A[j - 1], A[j] = A[j], A[j - 1]
        print(A)


if __name__ == '__main__':
    tes = [23, 34, 243, 1, 28, 7, 56]
    bubble_sort(tes)
