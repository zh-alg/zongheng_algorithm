# coding=utf-8

def getBit(num, i):
    return ((num & (1 << i)) != 0)


if __name__ == '__main__':
    print getBit(3, 1)
