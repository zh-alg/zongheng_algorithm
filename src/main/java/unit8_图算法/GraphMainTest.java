package unit8_图算法;

/**
 * Prim算法测试类
 */
public class GraphMainTest {
    public static void main(String[] args) {
        int n = 5;  //结点的个数
        String Vertexs[] = {"A", "B", "C", "D", "E"};
//		String Vertexs[] = {"1", "2", "3", "4", "5", "6", "7", "8"};

        //创建图对象
        Graph graph = new Graph(n);
        //循环的添加顶点
        for(String vertex: Vertexs) {
            graph.insertVertex(vertex);
        }

        //添加边
        //A-B A-C B-C B-D B-E
        graph.insertEdge(0, 1, 1); // A-B
        graph.insertEdge(0, 2, 1); //
        graph.insertEdge(1, 2, 1); //
        graph.insertEdge(1, 3, 1); //
        graph.insertEdge(1, 4, 1); //
        graph.insertEdge(2, 4, 1); //
//		//更新边的关系
//		graph.insertEdge(0, 1, 1);
//		graph.insertEdge(0, 2, 1);
//		graph.insertEdge(1, 3, 1);
//		graph.insertEdge(1, 4, 1);
//		graph.insertEdge(3, 7, 1);
//		graph.insertEdge(4, 7, 1);
//		graph.insertEdge(2, 5, 1);
//		graph.insertEdge(2, 6, 1);
//		graph.insertEdge(5, 6, 1);


        //显示邻结矩阵
        graph.showGraph();

        System.out.println("深度遍历:");
        graph.dfs(); // A->B->C->D->E [1->2->4->8->5->3->6->7]
//		System.out.println();
        System.out.println("\n广度优先:");
        graph.bfs(); // A->B->C->D-E [1->2->3->4->5->6->7->8]

    }
}
