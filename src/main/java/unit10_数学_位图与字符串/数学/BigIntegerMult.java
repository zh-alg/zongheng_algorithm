package unit10_数学_位图与字符串.数学;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BigIntegerMult {
    public static List<Integer> mul(List<Integer> listA, int b) {
        Collections.reverse(listA);
        int next = 0;
        List<Integer> listC = new ArrayList<>();
        for (int i = 0; i < listA.size(); i++) {
            next += listA.get(i) * b;
            listC.add(next % 10);
            next /= 10;
        }
        //进位可能超过10
        while (next != 0) {
            listC.add(next % 10);
            next /= 10;
        }
        Collections.reverse(listC);
        return listC;
    }

    public static void main(String[] args) {
        List lista = new ArrayList<>();
        lista.add(1);
        lista.add(2);
        lista.add(3);

        System.out.println((mul(lista, 9)));
    }
}
