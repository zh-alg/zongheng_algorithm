package unit10_数学_位图与字符串.数学;

import java.util.ArrayList;
import java.util.List;

public class BigIntegerSub {


    public static List<Integer> sub(List<Integer> listA, List<Integer> listB) {

        List<Integer> listC = new ArrayList<>();
        int next = 0;
        for (int i = 0; i < listA.size(); i++) {
            next = listA.get(i) - next;
            if (i < listB.size()) {
                next -= listB.get(i);
            }
            listC.add((next + 10) % 10);
            if (next < 0) {
                next = 1;
            } else {
                next = 0;
            }
        }
        return listC;
    }

    public static boolean cmp(List<Integer> listA, List<Integer> listB) {
        if (listA.size() != listB.size()) {
            return listA.size() > listB.size();
        }
        for (int i = listA.size() - 1; i >= 0; i--) {
            if (listA.get(i) != listB.get(i)) {
                return listA.get(i) > listB.get(i);
            }
        }
        return true;
    }


    public static void main(String[] args) {
        List listA = new ArrayList<>();
        listA.add(1);listA.add(2);listA.add(3);

        List listB = new ArrayList<>();
        listB.add(1);listB.add(2);listB.add(4);

        List<Integer> listC = new ArrayList<>();
        if (cmp(listA, listB)) {
            listC = sub(listA, listB);
            System.out.println(listC);
        } else {
            listC = sub(listB, listA);
            System.out.println("-" + listC);
        }
    }
}
