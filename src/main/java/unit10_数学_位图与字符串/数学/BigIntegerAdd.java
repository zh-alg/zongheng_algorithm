package unit10_数学_位图与字符串.数学;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BigIntegerAdd {
    public static List<Integer> add(List<Integer> listA, List<Integer> listB) {
        if (listA.size() < listB.size()) {
            return add(listB, listA);
        }
        int next = 0;
        List<Integer> listC = new ArrayList<>();
        for (int i = 0; i < listA.size(); i++) {
            next += listA.get(i);
            if (i < listB.size()) {
                next += listB.get(i);
            }
            listC.add(next % 10);
            next /= 10;
        }
        if (next != 0) {
            listC.add(next);
        }
        return listC;
    }

    public static void main(String[] args) {
        List lista = new ArrayList<>();
        lista.add(1);
        lista.add(2);
        lista.add(3);

        List listb = new ArrayList<>();
        listb.add(1);
        listb.add(2);
        listb.add(4);
        System.out.println((add(lista, listb)));
    }
}
