package unit10_数学_位图与字符串.数学;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BigIntegerDIv {
    public static List<Integer> div(List<Integer> listA, int b) {
        Collections.reverse(listA);
        List<Integer> listC = new ArrayList<>();
        int next = 0;
        for (int i = listA.size() - 1; i >= 0; i--) {
            next = next * 10 + listA.get(i);
            listC.add(next / b);
            next %= b;
        }
        return listC;
    }

    public static void main(String[] args) {
        List lista = new ArrayList<>();
        lista.add(1);
        lista.add(2);
        lista.add(0);
        System.out.println((div(lista, 5)));
    }
}
