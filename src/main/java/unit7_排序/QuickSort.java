package unit7_排序;

import javafx.print.Collation;

import java.util.*;

public class QuickSort {
    public static void main(String[] args) {
        int[] array = {6, 3, 2, 1, 4, 5, 8, 7};
//        int[] array = {8,4, 7,5,6,2,3,1};
        quickSort(array, 0, array.length - 1);
        System.out.println(Arrays.toString(array));
        List<Integer> list =new ArrayList<>();

    }

    private static void quickSort(int[] array, int start, int end) {
        if (start >= end) {
            return;
        }
        int left = start, right = end;
        int pivot = array[(start + end) / 2];
        while (left <= right) {
            while (left <= right && array[left] < pivot) {
                left++;
            }
            while (left <= right && array[right] > pivot) {
                right--;
            }
            if (left <= right) {
                int temp = array[left];
                array[left] = array[right];
                array[right] = temp;
                left++;
                right--;

            }
        }
        quickSort(array, start, right);
        quickSort(array, left, end);
    }
}
