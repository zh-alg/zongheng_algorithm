#include <stdio.h>
#include <stdlib.h>
#define MaxSize 10 //定义最大长度
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int Status;
typedef int ElemType;
typedef struct
{
    ElemType data[MaxSize];
    int length; // 线性表当前长度
} SqList;

void InitList(SqList *L)
{
    for (int i = 0; i < MaxSize; i++)
    {
        L->data[i] = i;
    }
    L->length = 0;
}

void printList(SqList *L)
{
    for (int i = 0; i < MaxSize; i++)
    {
        printf("data[%d]=%d \n ", i, L->data[i]);
    }
}
int main()
{

    SqList L;
    InitList(&L);
    printf("init data:\n");

    printList(&L);

    return 0;
}
