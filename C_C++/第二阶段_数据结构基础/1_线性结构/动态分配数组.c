#include <stdio.h>
#include <stdlib.h>
#define MaxSize 10 //定义最大长度
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int Status;
typedef int ElemType;
typedef struct
{
    int *data;
    int length; // 线性表当前长度
    int maxSize;
} SeqList;

//打印数组元素
void printList(SeqList *L)
{
    for (int i = 0; i < L->maxSize; i++)
    {
        printf("data[%d]=%d \n ", i, L->data[i]);
    }
}

//初始化数组的长度
void InitList(SeqList *L)
{
    L->data = (int *)malloc(MaxSize * sizeof(int));
    L->length = 0;
    L->maxSize = MaxSize;
    for (int i = 0; i < MaxSize; i++)
    {
        L->data[i] = i;
    }
    L->length = 0;
}

void IncreaseSize(SeqList *L, int len)
{
    int *p = L->data;
    L->data = (int *)malloc((L->maxSize + len) * sizeof(int));
    for (int i = 0; i < L->length; i++)
    {
        L->data[i] = p[i];
    }
    L->maxSize = L->maxSize + len;
}

int LocateElem(SeqList *L, int e)
{
    for (int i = 0; i < L->maxSize; i++)
    {
        if (L->data[i] == e)
        {
            return i + 1;
        }
    }
    return 0;
}

//在指定位置插入元素
//表示的是第几个，从1开始编号
Status ListInsert(SeqList *L, int i, ElemType e)
{
    int k;
    if (L->length == L->maxSize)
    {
        return ERROR;
    }
    if (i < 1 || i > L->length + 1)
    {
        return ERROR;
    }
    for (k = L->length - 1; k >= i - 1; k--)
    {
        L->data[k + 1] = L->data[k];
    }
    L->data[i - 1] = e;
    L->length++;
    return OK;
}
//删除元素
Status ListDelete(SeqList *L, int i)
{
    if (i < 1 || i > L->length)
    {
        return FALSE;
    }
    int e = L->data[i - 1];
    for (int i = 0; i < L->length; i++)
    {
        L->data[i - 1] = L->data[i];
    }
    L->length--;
    return 0;
}

int main()
{

    SeqList L;
    InitList(&L);
    printf("init data:\n");
    // IncreaseSize(&L, 5);
    ListInsert(&L, 1, 3);
    ListDelete(&L, 1);

    printList(&L);
    // printf("get at %d\n",LocateElem(&L,3));
    return 0;
}
