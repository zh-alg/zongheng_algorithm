#include <stdio.h>
main()
{
    int n1 = 15;
    short n2 = 15;
    unsigned short n3 = 15;
    unsigned char c = 15;
    n1 <<= 15;
    n2 <<= 15;
    n3 <<= 15;
    c <<= 6;
    printf("n1=%x,n2=%d,n3=%d,c=%x,c<<4=%d", n1, n2, n3, c, c << 4);
}